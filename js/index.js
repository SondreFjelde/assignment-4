let bankBalance = 0;
let loanBalance = 0;
let salary = 0;

/// store
const pcSelectEl = document.getElementById("pc-select");
const repayLoanButton = document.getElementById("repay-button");
const loanBalanceEl = document.getElementById("loan-balance");

let pcs = [];
let cart = [];
let pay = 0;



fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => pcs = data)
    .then(pcs => addPcsToMenu(pcs))

const addPcsToMenu = (pcs) => {
    pcs.forEach(x => addPcToMenu(x));
}

const addPcToMenu = (pc) => {
    const pcEl = document.createElement("option");
    pcEl.value = pc.id;
    pcEl.appendChild(document.createTextNode(pc.title));
    pcSelectEl.appendChild(pcEl);
}

let activePc;
const handlePcMenuChange = e => {
    const selected = pcs[e.target.selectedIndex];
    console.log(selected);
    activePc = selected;
} 

pcSelectEl.addEventListener("change", handlePcMenuChange)

const showPcButton = document.getElementById("show-pc");
const itemImg = document.getElementById("item-image")
const itemTitle = document.getElementById("item-title");
const itemDesc = document.getElementById("item-desc");
const itemPrice = document.getElementById("item-price");

showPcButton.onclick = () => {
    itemTitle.innerText = activePc.title;
    itemDesc.innerText = activePc.description
    itemPrice.innerText = activePc.price;
    itemImg.src = "https://noroff-komputer-store-api.herokuapp.com/" + activePc.image;
}

const itemBuy = document.getElementById("item-buy-button");

const handleBuyPc = () => {
    const price = parseInt(activePc.price);
    
    if (bankBalance - price < 0) {
        alert("sorry, you can't afford that");
        return;
    }
    else {
        bankBalance = bankBalance - price
        return;
    }

    
    alert("Congratulations, you've now bought " + activePc.title)
}

itemBuy.addEventListener("click", handleBuyPc);

///  bank
const addBankBalance = (add) => {
    bankBalance = bankBalance + add;
    document.getElementById("bank-balance").innerHTML = bankBalance;
    return bankBalance;
}

const loan = () => {
    // if user already has a loan, refuse this newLoan
    if (loanBalance > 0) {
        alert("You're not allowed to take out a new loan before repaying the last")
        return;
    }
    //input loan sum and convert commas
    let newLoan = prompt("How much would you like to loan?");
    newLoan = newLoan.replace(",", ".");
    //check if loan newLoan is a number
    if (typeof(parseFloat(newLoan)) != "number") {
        alert("please input a valid sum");
        return;
    }
    newLoan = parseFloat(newLoan);
    // check if user meets the loan sum requirements
    if (newLoan > 2*bankBalance) {
        alert("Loan amount is more than what is allowed for your balance")
        return;
    }
    loanBalance = loanBalance + parseFloat(newLoan);
    bankBalance = bankBalance + loanBalance;
    document.getElementById("loan-balance").innerText = loanBalance;
    document.getElementById("bank-balance").innerText = bankBalance;
    
    toggleButtonVisibility();
    return loanBalance;
}

const repayLoan = e => {
    if (loanBalance <= 0) {alert("no loan");return;}
    let repayment = prompt("Enter downpayment amt");
    repayment = repayment.replace(",", ".");
    try { repayment = parseFloat(repayment) }
    catch (e) {
        console.log(e);
        return;
    }
    if ((loanBalance - repayment) < 0) return;
    document.getElementById("loan-balance").innerText = loanBalance - repayment;
    document.getElementById("salary-balance").innerText = salary - repayment;
    salary = salary - repayment;
    loanBalance = loanBalance - repayment;
    toggleButtonVisibility();
    return repayment;
}

function addSalary() {
    salary = salary + 100;
    document.getElementById("salary-balance").innerHTML = salary;
    return salary;
}

function depositSalary() {
    let deposit = parseInt(document.getElementById("salary-balance").innerHTML);
    if (deposit <= 0) {
        return;
    }

    addBankBalance(deposit);
    document.getElementById("salary-balance").innerHTML = 0;
    salary = 0;
    return deposit;
}

function toggleButtonVisibility(e) {
    if (loanBalance == 0) {
        document.getElementById("repay-button").style.display = "none";
        return
    }
        
    document.getElementById("repay-button").style.display = "inline-block";
}

toggleButtonVisibility();